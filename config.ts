/**
 * Hey! Welcome to our config. Here can you setup some stuff like port, package location and
 * user stuff. Good luck!
 * 
 * For database configuration, take a look at ormconfig.json (in this folder).
 */
export const config: any = {
    /* If enabled, developer messages get printed out. Can be very useful while development
       but very annoying while production. (default: false) */
    debug: true,
    
    /* If enabled, you see when a package/repository get downloaded. */
    download: true,

    /* If enabled, you see when a package get uploaded. */
    upload: true,

    /* Port of Express where all applications have to access to it. (default: 80) */
    port: 1337,

    /* You can create dummy data like packages and users. (default: false)
       WARNIG: THIS WILL DROP THE DATABASE `inpm`! ALL YOUR DATA IN IT WILL BE LOST!
    */
    dummys: true,

    /* Keep this key secret. Every login will be signed with this key.
       NOTE: If you change this key, each login will be invaild and every user
             has to login again. */
    jwtKey: "better_nobody_know_this_key_except_you_,_I_dare_you!",

    /* How many salt rounds to hash a password. Keep this value low but as high as possible!
       Take a look at https://www.npmjs.com/package/bcrypt#a-note-on-rounds.
       (default: 12) */
    saltRounds: 5,

    /* Path where all repository's (.inr) lives in. */
    repositorys: "./repositorys/",

    /* Path where all checksum's (.sha256) lives in. */
    checksums: "./checksums/",

    /* Path where all screenshots (.png) lives in. */
    screenshots: "./screenshots/",

    /* Path where all icon (.png) lives in. */
    icons: "./icons/"
}