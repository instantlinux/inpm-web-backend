# The Backend
Welcome in the spooky place of the inpm-webpanel, a world full of suffering, screaming and bugs.

## Setup
1. After cloning this project, you go to `backend/`.
2. Edit `config.js` (change the JWT key, port or sql data)
3. Now you're ready to go with `sudo node app.js`.
4. Go to `frontend/` and do `ng serve` to start our frontend.

## Test
On chrome, you can install [Servistate](https://chrome.google.com/webstore/detail/servistate-http-editor-re/mmdjghedkfbdhbjhmefbbgjaihmmhkeg?hl=de) to test this API.

1. Click on `Quick Request`

2. Then choose your request type (take a look into `backend/lib/routes.js` for your request type)

3. Enter `localhost:PORT/ENDPOINT_NAME` (replace PORT and ENDPOINT_NAME(`auth`, `createuser`, ...))

4. On POST, click on `RAW` and click on `TEXT` and choose `JSON`.

5. Enter the body content below (take a look into `backend/sites/...` for the body content)

6. Click on `Set appropriate Content-Type header`.

7. Finally, click on `Send Request` and If you do everything right it will return something.

On firefox, I don't know...