import { System } from "../models/system";

export const GET_rinfo: Function = async (req, res) => {
    const repo = req.params.repo;
    
    if(repo == 'system') {
        const db: System[] = await System.find({
            select: ['name', 'version', 'description', 'releasedAt', 'updatedAt',
                    'maintainer', 'license', 'tags', 'downloads'],
            relations: ['ratings']
        });
        
        // Simplify ratings. Remove comments and stuff and only keeps the rating.
        for(let i: number = 0; i < db.length; i++) {
            let newRatings: Array<any> = [];
            for(let x: number = 0; i <= db[i].ratings.length; x++) {
                try {
                    newRatings.push(db[i].ratings[x].rating)
                } catch(err) { break }
            }
            db[i].ratings = newRatings;
        }

        res.status(200).send(db)
    }
}