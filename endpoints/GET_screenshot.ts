import * as fs from 'fs';
import * as path from 'path';
import { config } from '../config';

export const GET_screenshot: Function = async (req, res) => {
    const repo = req.params.repo;
    const pkg = req.params.package;
    const index = req.params.index;

    if(repo == undefined || pkg == undefined || index == undefined) {
        res.stauts(400).send()
        return;
    }

    try {
        res.status(200).sendFile(path.resolve("./" + config.screenshots + "/" + pkg + "/" + index + ".png"))
    } catch(err) {
        res.status(500).send();
    }
}