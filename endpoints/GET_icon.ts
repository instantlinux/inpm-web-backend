import * as fs from 'fs';
import * as path from 'path';
import { config } from '../config';

export const GET_icon: Function = async (req, res) => {
    const repo = req.params.repo;
    const pkg = req.params.package;

    if(repo == undefined || pkg == undefined) {
        res.stauts(400).send()
        return;
    }

    try {
        res.status(200).sendFile(path.resolve("./" + config.icons + "/" + repo + "/" + String(pkg).toLowerCase() + ".png"))
    } catch(err) {
        res.status(500).send();
    }
}