import { Users } from "../models/users";
import * as jwt from 'jsonwebtoken';
import { now, logger } from "../lib/logger";
import { config } from "../config";
import * as chalk from "chalk";
import * as bcrypt from 'bcryptjs';

export const POST_auth: Function = async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    // Send 400 If not all parameters are given.
    if(username == undefined || password == undefined) {
        res.status(400).send();
        return;
    }

    // Search for user in database.
    const db: Users = await Users.findOne({
        where: {
            username: username
        }
    })

    // If user didn't exist.
    if(db == undefined) {
        res.status(404).send();
        return;
    }

    // Check password.
    const pass = await bcrypt.compare(password, db.password);
    if(pass) {
        const token = jwt.sign({
            id: db.id,
            username: db.username,
            isDeveloper: db.isDeveloper,
            isAdmin: db.isAdmin,
            loggedIn: now()
        }, config.jwtKey, {
            expiresIn: "7d"
        })
        logger("User " + chalk.default.bold(db.username) + " is now " + chalk.default.green("authorized") + ".");
        res.status(200).send({
            token: token,
	    username: db.username,
	    id: db.id,
            isDeveloper: db.isDeveloper,
            isAdmin: db.isAdmin,
            loggedIn: now()
        })

        db.lastLogin = now();
        await db.save();
    } else {
        res.status(401).send();
    }

}
