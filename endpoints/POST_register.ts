import { Users } from "../models/users";
import * as bcrypt from 'bcryptjs';
import { config } from "../config";
import { now, logger, _MODES } from "../lib/logger";
import chalk from "chalk";

export const POST_register: Function = async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;

    // Check If all parameters are given.
    if(username == undefined || password == undefined || email == undefined) {
        res.status(400).send();
        return;
    }

    // Check If username already exists.
    if(await Users.findOne({
        where: {
            username: username
        }
    })) {
        res.status(409).send("Username already in use!")
        return;
    }

    // Check If email already exisis.
    if(await Users.findOne({
        where: {
            email: email
        }
    })) {
        res.status(409).send("E-Mail already in use!")
        return;
    }

    // Hash password
    let salt = bcrypt.genSaltSync(config.saltRounds);
    let hash = bcrypt.hashSync(password, salt);
    let hashedPassword = hash;

    // Create user If everything is available.
    const newUser = Users.create({
        username: username,
        password: hashedPassword,
        email: email,
        status: '*',
        isDeveloper: false,
        isAdmin: false,
        createdAt: now(),
        activatedAt: null,
        lastLogin: null,
        bannedAt: null
    })

    await newUser.save();
    logger("Created user " + chalk.bold(username) + " (" + newUser.id + ").", _MODES.SUCCESS)
    res.status(200).send()
}