import * as fs from 'fs';
import * as path from 'path';
import { config } from '../config';
import { logger, _MODES } from '../lib/logger';
import chalk from 'chalk';
import { System } from '../models/system';

export const GET_package: Function = async (req, res) => {
    const repo = req.params.repo;
    const pkg = req.params.package;

    // Try to find file.
    let file = path.resolve(config.repositorys + req.params.repo + "/" + pkg + ".inp")
    
    try {
        fs.readFileSync(file)
    } catch(err) {
        res.status(404).send("<p>Given package is not here! Are you on the right server? Maybe someone else have your package.</p>")
        return;
    }
    logger("Someone downloads the package " + chalk.bold(pkg) + " from " + chalk.bold(repo) + ".", _MODES.DOWNLOAD);
    
    if(repo == "system") {
        const found = await System.findOne({ where: { name: pkg } });
        found.downloads++;
        await found.save();
    }
    
    res.status(200).sendFile(file);
}