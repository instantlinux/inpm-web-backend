import { logger, _MODES, now } from "../lib/logger";
import chalk from "chalk";
import { System } from "../models/system";
import { Users } from "../models/users";
import * as jwt from 'jsonwebtoken';
import { SQLITE } from "../app";
import { Pkgs } from "../models/sqlite/pkgs";
import { Repository, getConnectionOptions, getConnection } from "typeorm";

export const POST_uploadPkg: Function = async (req, res) => {
    console.log("Here!");
    
    const pkg: string                   = req.body.package;
    const repo: string                  = req.params.repo;
    const version: string               = req.body.version;
    const description: string           = req.body.description;
    const license: string               = req.body.license;
    const screenshots: Array<string>    = req.body.screenshots;
    const decoded: any                  = jwt.decode(req.headers.token);
    const tags: Array<string>           = req.body.tags;
    const dependencies: Array<string>   = req.body.dependencies;
    const maintainer: Users            = await Users.findOne({ where: { name: decoded.username } });

    logger("File " + chalk.bold(req.file.originalname) + " (" + (req.file.size/1024/1000).toFixed(2) + " MB) for " + chalk.bold(pkg) + " got uploaded!", _MODES.UPLOAD)

    if(repo == "system") {
        /*
        await System.create({
            name: pkg,
            version: version,
            description: description,
            license: license,
            screenshots: screenshots,
            maintainer: maintainer,
            tags: tags,
            dependencies: dependencies,
            downloads: 0,
            views: 0,
            releasedAt: now(),
            updatedAt: null,
            ratings: null
        }).save();*/
    }

    getConnection("system").getRepository(Pkgs).create({
        name: pkg,
        version: version,
        description: description,
        license: license,
        maintainer: decoded.username,
        dependencies: dependencies
    }).save()

    logger("Package " + chalk.bold(pkg) + " in " + chalk.bold(repo) + " by " + chalk.green(chalk.bold(decoded.username)) + " got saved!", _MODES.SUCCESS);
        
    res.status(200).send()
}