import * as fs from 'fs';
import * as path from 'path';
import { config } from '../config';
import { logger, _MODES } from '../lib/logger';
import chalk from 'chalk';

export const GET_repository: Function = (req, res) => {
    let file = path.resolve(config.repositorys + req.params.repo + ".inr")
    try {
        fs.readFileSync(file)
    } catch(err) {
        res.status(404).send("<p>Given repository is not here! Are you on the right server? Maybe someone else have your repository.</p>")
        return;
    }
    logger("Someone download's the repository " + chalk.bold(req.params.repo) + ".", _MODES.DOWNLOAD)
    res.status(200).sendFile(file);
}