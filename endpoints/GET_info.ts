import { System } from "../models/system";

export const GET_info: Function = async (req, res) => {
    const repo = req.params.repo;
    const pkg = req.params.package;  

    if(repo == "system") {
        const db = await System.findOne({
            where: {
                name: pkg
            },
            relations: ['ratings', 'ratings.user', 'maintainer', 'screenshots']
        })

        if(db == undefined) {
            res.status(404).send("<p>Given package is not here! Are you on the right server? Maybe someone else have your package.</p>")
            return;
        }
        
        let clone:any = db;

        // Edit result
        let maintainer = db.maintainer.username;
        clone.maintainer = maintainer;
        for(let i: number = 0; i < clone.ratings.length; i++) {
            clone.ratings[i].user = clone.ratings[i].user.username;
        }

        res.status(200).send(clone)

        const found = await System.findOne({ where: { name: pkg } });
        found.views++;
        await found.save();
    } else {
        res.status(404).send("Unknown repository")
    }
}