import * as chalk from 'chalk';
import * as express from 'express';
import * as cors from 'cors';
import * as bodyp from 'body-parser';
import * as multer from 'multer';
import { now, logger, _MODES } from "./lib/logger";
import { createConnection, Connection } from 'typeorm';
import { config } from './config';
import { seed } from './lib/seeder';
import { Routes } from './lib/routes';
import { SystemComments } from './models/system_comments';
import { SystemScreenshots } from './models/system_screenshots';
import { System } from './models/system';

export const VERSION = "1.0.0";
export const STARTED = Date.now();
export let DONE = 0;

export let CONNECTION: Connection = null;
export let SQLITE: Connection = null;

    
const app = express()

function done() {
    DONE = Date.now();
    logger("Started within " + chalk.default.bold((DONE-STARTED) + "ms") + "!")
}

async function run() {
    console.log(chalk.default.blue("inPM backend started at " + chalk.default.bold(now())) + 
    "\n-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-\n");
    try {
        CONNECTION = await createConnection("default");
    } catch(err) {
        logger("Failure while connection to database: " + err, _MODES.ERROR)
    }
    logger("Connection to database got established!", _MODES.SUCCESS)

    try {
        SQLITE = await createConnection("system")
    } catch (err) {
        logger("Failure while connection to system repository: " + err, _MODES.ERROR)
    }
    logger("Connection to system database got established!", _MODES.SUCCESS)

    app.use(cors());
    app.use(bodyp.urlencoded({ extended: false }));
    app.use(bodyp.json())
    Routes(app)

    await app.listen(config.port, null)

    // Start seeding If it's enabled.
    if(config.dummys) {
        await seed();
    }
    
    logger("inPM is now listening on port " + config.port)
    done();
}

run()