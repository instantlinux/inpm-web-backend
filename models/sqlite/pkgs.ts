import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from "typeorm";

@Entity('pkgs')
export class Pkgs extends BaseEntity {

    @PrimaryColumn({
        type: 'varchar',
        length: 32
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 16
    })
    version: string;

    @Column('text')
    description: string;

    @Column({
        type: 'varchar',
        length: 10
    })
    license: string;

    @Column({
        type: 'varchar',
        length: 16
    })
    maintainer: string;

    @Column('simple-array')
    dependencies: Array<string>;

}