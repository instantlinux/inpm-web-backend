import { BaseEntity, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToMany } from "typeorm";
import { System } from "./system";

@Entity('system_screenshots')
export class SystemScreenshots extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('tinyint')
    index: number;

    @Column({
        type: 'varchar',
        length: 64
    })
    title: string;

    @Column('text')
    description: string;

    @ManyToOne(type => System, sys => sys.screenshots)
    package: System;

}