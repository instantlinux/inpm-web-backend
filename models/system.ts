import { BaseEntity, Entity, PrimaryColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { Users } from "./users";
import { Dependencies, Ratings } from "../lib/interfaces";
import { SystemComments } from "./system_comments";
import { SystemScreenshots } from "./system_screenshots";

@Entity('system')
export class System extends BaseEntity {

    @PrimaryColumn({
        type: 'varchar',
        length: 32
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 16
    })
    version: string;

    @Column('text')
    description: string;

    @Column({
        type: 'varchar',
        length: 10
    })
    license: string;

    @ManyToOne(type => Users, users => users.system_packages, {
        onDelete: 'CASCADE'
    })
    maintainer: Users;

    @Column('simple-array')
    tags: Array<string>

    @Column('simple-array')
    dependencies: Array<string>;

    @Column('int')
    downloads: number;

    @Column('int')
    views: number;

    @Column('datetime')
    releasedAt: string;

    @Column({
        type: 'datetime',
        nullable: true
    })
    updatedAt: string;

    @OneToMany(type => SystemComments, sysc => sysc.package)
    ratings: SystemComments[];

    @OneToMany(type => SystemScreenshots, syss => syss.package)
    screenshots: SystemScreenshots[];

}