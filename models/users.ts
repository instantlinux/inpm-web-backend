import { BaseEntity, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, ManyToOne, BeforeInsert, BeforeUpdate } from "typeorm";
import * as bcrypt from 'bcryptjs';
import { System } from "./system";
import { SystemComments } from "./system_comments";

@Entity('users')
export class Users extends BaseEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 16
    })
    username: string;

    @Column('varchar')
    password: string;

    @Column('varchar')
    email: string;

    @OneToMany(type => System, system => system.maintainer)
    system_packages: System[]

    @Column({
        type: 'varchar',
        length: 2
    })
    status: string;

    @OneToMany(type => SystemComments, comment => comment.id)
    comments: SystemComments[]

    @Column('tinyint')
    isDeveloper: boolean;

    @Column('tinyint')
    isAdmin: boolean;

    @Column('datetime')
    createdAt: string;

    @Column({
        type: 'datetime',
        nullable: true
    })
    lastLogin: string;

    @Column({
        type: 'datetime',
        nullable: true
    })
    activatedAt: string;

    @Column({
        type: 'datetime',
        nullable: true
    })
    bannedAt: string;

    @BeforeInsert()
    async hash() {
        bcrypt.genSalt(30, (err, salt) => {
            bcrypt.hash(this.password, salt, (err, hash) => {
                this.password = hash;
            })
        })
    }
}