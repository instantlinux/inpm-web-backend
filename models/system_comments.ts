import { BaseEntity, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Users } from "./users";
import { System } from "./system";

@Entity('system_comments')
export class SystemComments extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Users, user => user.comments)
    user: Users;

    @ManyToOne(type => System, sys => sys.ratings)
    package: System;

    @Column('float')
    rating: number;

    @Column({
        type: 'varchar',
        length: 255
    })
    comment: string;

    @Column('tinyint')
    edited: boolean;

    @Column({
        type: 'datetime',
        nullable: true
    })
    editedTimestamp: string;

    @Column('datetime')
    timestamp: string;

}