import { BaseEntity, Entity, PrimaryColumn, Column, ManyToOne } from "typeorm";

@Entity('tags')
export class Tags extends BaseEntity {

    @PrimaryColumn({
        type: 'varchar',
        length: 26
    })
    tag: string;

    @Column('text')
    description: string;

    @Column({
        type: 'varchar',
        length: 7
    })
    background: string;

    @Column({
        type: 'varchar',
        length: 7
    })
    foreground: string;

}