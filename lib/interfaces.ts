import { Users } from "../models/users";

export interface Ratings {
    user: number,
    rating: number,
    comment: string,
    edited: boolean,
    editedTimestamp: string,
    timestamp: string
}

export interface Dependencies {
    repository: string,
    package: string,
    version: string
}

export interface _Repository {
    name: string;
    description: string;
    license: string;
    screenshots: Array<string>,
    maintainer: Users,
    dependencies: Array<Dependencies>;
    content: Array<string>;
    downloads: number;
    ratings: Ratings
}