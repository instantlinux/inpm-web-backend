export const _system: Array<any> = [
    {
        name: "foobar",
        version: "2.1.0",
        description: "This program has a little bit of foo and bar. Try it!",
        license: "MIT",
        releasedAt: "2018-07-02 16:52:30",
        updatedAt: "2018-07-06 11:00:42",
        maintainer: "Mondei1",
        dependencies: [],
        downloads: 6820,
        views: 9082,
        tags: ['Library'],
        ratings: [
            {
                user: "GuidedLinux",
                rating: 4.5,
                comment: "I will be better If you add \"No Code\": https://github.com/kelseyhightower/nocode",
                edited: false,
                editedTimestamp: null,
                timestamp: "2018-07-02 17:04:47"
            },
            {
                user: "EvilMorty",
                rating: 0,
                comment: "This is crap! Nothing works. Every baby can code better then you!",
                edited: true,
                editedTimestamp: "2018-06-04 14:09:27",
                timestamp: "2018-07-04 14:13:09"
            },
            {
                user: "FooBar",
                rating: 3.5,
                comment: "Sure, it's your first package but can you add this and this, then it will be much better.\n\nBut it's good yet and nice that you mentioned my in the title!",
                edited: false,
                editedTimestamp: null,
                timestamp: "2018-07-04 15:56:32"
            }
        ],
        screenshots: [
            {
                index: 0,
                title: "Home",
                description: "Here you see the home screen of the application."
            },
            {
                index: 1,
                title: "On work",
                description: "This is foobar at work."
            }
        ]
    },
    {
        name: "fireup",
        version: "0.1.5",
        description: "Makes instantLinux much faster! Faster then yet.",
        license: "GPL-3.0",
        releasedAt: "2018-07-05 13:28:12",
        updatedAt: null,
        maintainer: "FooBar",
        dependencies: ['foobar'],
        tags: ['Office'],
        downloads: 490,
        views: 1082,
        ratings: [
            {
                user: "FooBar",
                rating: 3.5,
                comment: "I noticed a difference of 200ms, it didn't make much sence.",
                edited: false,
                editedTimestamp: null,
                timestamp: "2018-07-02 17:04:47"
            },
            {
                user: "EvilMorty",
                rating: 0,
                comment: "Shame on you! Only 200ms? I'll get 3.5sek with my program, bit**!!",
                edited: true,
                editedTimestamp: "2018-06-04 14:09:27",
                timestamp: "2018-07-04 14:13:09"
            },
            {
                user: "Mondei1",
                rating: 5,
                comment: "Wow! I didn't need a SSD anymore! Please make more!",
                edited: false,
                editedTimestamp: null,
                timestamp: "2018-07-04 15:56:32"
            }
        ],
        screenshots: []
    },
    {
        name: "SuperTuxKart",
        version: "0.9.3-1",
        description: "Supertuxkart is a free 3D kart racing game, with a focus on having fun over realism. You can play with up to 4 friends on one PC, racing against each other, or try to beat the computer in single-player mode.",
        license: "GPL-3.0",
        releasedAt: "2018-07-05 13:28:12",
        updatedAt: null,
        maintainer: "FooBar",
        dependencies: ['libbluetooth3', 'libc6', 'libcurl3-gnutls', 'libenet7'],
        tags: ['Games'],
        downloads: 9302,
        views: 1592,
        ratings: [
            {
                user: "FooBar",
                rating: 3.5,
                comment: "Ideales Spiel für mehrere Spieler, tolle neue Levels, viele zusätzliche Inhalte herunterladbar. Macht auch alleine Spaß.",
                edited: false,
                editedTimestamp: null,
                timestamp: "2018-03-13 17:04:47"
            }
        ],
        screenshots: []
    }
]