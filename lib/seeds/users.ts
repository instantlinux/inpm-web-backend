export const _users: Array<any> = [
    {
        username: "Mondei1",
        password: "123",
        email: "emailchecker000@gmail.com",
        status: "+",
        isDeveloper: true,
        isAdmin: true,
        createdAt: "2018-07-02 16:25:40",
        activatedAt: "2018-07-02 16:27:13",
        bannedAt: null
    },
    {
        username: "GuidedLinux",
        password: "abc",
        email: "projects@guidedlinux.org",
        status: "+",
        isDeveloper: true,
        isAdmin: true,
        createdAt: "2018-07-02 16:29:55",
        activatedAt: "2018-07-02 17:32:04",
        bannedAt: null
    },
    {
        username: "FooBar",
        password: "foobar",
        email: "foo@bar.com",
        status: "+",
        isDeveloper: false,
        isAdmin: false,
        createdAt: "2018-07-02 16:25:40",
        activatedAt: null,
        bannedAt: null
    },
    {
        username: "EvilMorty",
        password: "the_devil",
        email: "morty@rick.space",
        status: "-2",
        isDeveloper: false,
        isAdmin: false,
        createdAt: "2018-07-03 18:35:13",
        activatedAt: "2018-07-03 18:38:09",
        bannedAt: "2018-07-12 09:47:12"
    },
]