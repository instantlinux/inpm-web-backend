import * as chalk from 'chalk';
import { config } from '../config';
import { SSL_OP_CRYPTOPRO_TLSEXT_BUG } from 'constants';

/* Contains all modes */
export enum _MODES {
    NORMAL,
    DEBUG,
    ERROR,
    SUCCESS,
    HINT,
    DOWNLOAD,
    UPLOAD
}
/* Return's YYYY-MM-DD HH:mm:SS for mysql*/
export const now: Function = () => {
    let date = new Date();
    let year: string = String(date.getFullYear());
    let month: string = String(date.getMonth());
    let day: string = String(date.getDay());
    let hours: string = String(date.getHours());
    let minutes: string = String(date.getMinutes());
    let seconds: string = String(date.getSeconds());

    if(month.length == 1) month = "0" + month;
    if(day.length == 1) day = "0" + day;
    if(hours.length == 1) hours = "0" + hours;
    if(minutes.length == 1) minutes = "0" + minutes;
    if(seconds.length == 1) seconds = "0" + seconds;

    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}

/* Prints a message with a mode like Normal, Error, Success, Debug, ... */
export const logger: Function = (message: string, mode: _MODES) => {
    let toPrint = "[" + now() + "] ";
    if(mode == _MODES.NORMAL || mode == undefined || mode == null) {
        toPrint += (chalk.default.gray("MESSAGE: ") + message);
    } else if(mode == _MODES.SUCCESS) {
        toPrint += (chalk.default.green("SUCCESS: ") + message);
    } else if(mode == _MODES.ERROR) {
        toPrint += (chalk.default.red("ERROR: ") + message);
    } else if(mode == _MODES.HINT) {
        toPrint += (chalk.default.yellow("HINT: ") + message);
    } else if(mode == _MODES.DEBUG) {
        if(config.debug) toPrint += chalk.default.cyan("DEBUG: ") + message;
    } else if(mode == _MODES.DOWNLOAD) {
        if(config.download) toPrint += chalk.default.magenta("DOWNLOAD: ") + message;
    } else if(mode == _MODES.UPLOAD) {
        if(config.upload) toPrint += chalk.default.magentaBright("UPLOAD: ") + message;
    }
    
    if(!config.debug && mode != _MODES.DEBUG) {
        // Print message If debug is disabled and mode isn't debug.
        console.log(toPrint);
    } else if(config.debug && mode == _MODES.DEBUG) {
        // Print message If debug is enabled and mode is debug.
        console.log(toPrint);
    } else if(!config.debug && mode == _MODES.DEBUG) {
        // Return, If message is debug and debug is disabled. 
        return;
    } else {
        console.log(toPrint)
    }
}
