import { GET_repository } from "../endpoints/GET_repository";
import { POST_auth } from "../endpoints/POST_auth";
import { POST_register } from "../endpoints/POST_register";
import { GET_package } from "../endpoints/GET_package";
import { GET_info } from "../endpoints/GET_info";
import * as multer from 'multer';
import { POST_uploadPkg } from "../endpoints/POST_uploadPkg";
import { logger, _MODES } from "./logger";
import * as chalk from 'chalk';
import { GET_rinfo } from "../endpoints/GET_rinfo";
import { GET_screenshot } from "../endpoints/GET_screenshot";
import { GET_icon } from "../endpoints/GET_icon";

export const Routes: Function = (app) => {
    const upload = multer({
        dest: '/tmp/',
        limits: {
            files: 1,
            fields: 8,
            fileSize: 1000000000 // 1gb
        },
        onFileUploadStart: (file) => {
            logger("Start file upload " + chalk.default.bold(file.filename) + " get uploaded!", _MODES.UPLOAD)
            return true;
        }
    });

    app.get("/", (req, res) => {
        res.status(200).send("<h1>Hey user!</h1>\n" +
        "<p>This is the backend for inPM. But this here isn't a place for users. <a href=\"https://instantlinux.org/inpm\">Go back</a>.</p>")
    })

    /* User auth */
    app.post('/auth', (req, res) => POST_auth(req, res));
    app.post('/register', (req, res) => POST_register(req, res));

    /* Repository */
    app.get("/:repo", (req, res) => GET_repository(req, res))
    app.get("/:repo/info", (req, res) => GET_rinfo(req, res))

    /* Packge */
    app.get("/:repo/:package", (req, res) => GET_package(req, res))
    app.get("/:repo/:package/info", (req, res) => GET_info(req, res))
    app.get("/:repo/:package/screenshot/:index", (req, res) => GET_screenshot(req, res))
    app.get("/:repo/:package/icon", (req, res) => GET_icon(req, res))
    app.post("/:repo/upload",upload.single('pkg'), (req, res) => POST_uploadPkg(req, res))

    app.get("**", (req, res) => {
        res.status(200).send("<h1>404 Not Found</h1><p>This site doesn't exist! Is this a typo?</p>")
    })
}