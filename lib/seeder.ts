import { _users } from "./seeds/users";
import { logger, _MODES } from "./logger";
import { Users } from "../models/users";
import { _system } from "./seeds/system";
import { System } from "../models/system";
import { getConnection } from "typeorm";
import { CONNECTION } from "../app";
import * as bcrypt from 'bcryptjs';
import { config } from "../config";
import { SystemComments } from "../models/system_comments";
import chalk from "chalk";
import { SystemScreenshots } from "../models/system_screenshots";
import { connect } from "tls";

export const seed: Function = async () => {
    logger("===================== [ " + chalk.bold("SEEDER") + " ] =====================")
    logger("Database dropped!", _MODES.SUCCESS);
    await CONNECTION.synchronize(true)

    // Users
    logger("Start to create dummy users ...")
    for(let i: number = 0; i < _users.length; i++) {
        let element = _users[i];
        // Hash password
        let salt = bcrypt.genSaltSync(config.saltRounds);
        let hash = bcrypt.hashSync(element.password, salt);
        element.password = hash;

        // Save to db
        await CONNECTION.getRepository(Users).save(element);
        logger("Created user " + (i+1) + " (" + element.username + ") of " + _users.length + " ...", _MODES.SUCCESS);
    }

    // Repository system
    logger("Start to create dummy repository 'system' ...")
    for(let i: number = 0; i < _system.length; i++) {
        let element = _system[i];

        // Convert username string to User.
        element.maintainer = await Users.findOne({
            where: {
                username: element.maintainer
            }
        })
        // Convert comment users to a User.
        for(let x: number = 0; i < element.ratings; i++) {
            element.ratings[i].user = await Users.findOne({
                where: {
                    username: element.ratings[i].user
                }
            })
        }

        // Save to db
        const db = await CONNECTION.getRepository(System).save(element);
        logger("Package " + element.name + " in system got created!", _MODES.SUCCESS);

        // Save comments
        for(let x: number = 0; x < element.ratings.length; x++) {
            const comment = await SystemComments.create({
                user: await Users.findOne({
                    where: {
                        username: element.ratings[x].user
                    }
                }),
                package: db,
                rating: element.ratings[x].rating,
                comment: element.ratings[x].comment,
                edited: element.ratings[x].edited,
                editedTimestamp: element.ratings[x].editedTimestamp,
                timestamp: element.ratings[x].timestamp
            });
            await comment.save()
            logger("Comment " + comment.id + " by " + comment.user.username + " got saved!", _MODES.DEBUG)
    
        }

        // Save screenshots
        for(let x: number = 0; x < element.screenshots.length; x++) {
            let clone = element.screenshots[x];
            clone.package = await System.findOne({ where: { name: element.name } })
            const screenshot = SystemScreenshots.create(clone);
            
            await screenshot.save();

            logger("Screenshot " + screenshot.id + " for " + screenshot.package.name + " got saved!", _MODES.DEBUG)
        }
    }

    logger("======================================================")

    return 0;
}